# PODS TincyMCE include

Adds an option to insert a template through a shortcode via TincyMCE editor

### How to use
Add a comment doc block in with `(Type: Name)` as the structure
```
thm-name/templates/**/*.php

---

ie. theme/templates/components/card.php

...

<?php /** (Component: Card) */ ?>

<div>
    My card component
</div>
```
