<?php
/**
 * Plugin Name: PODS TinyMCE Include
 * Description: WP plugin to include template files in TinyMCE
 * Version: 1.0.0
 * Author: PODS
 * Author URI: http://nupods.net
 * Text Domain: pods
 */

namespace PODS;


// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


class TinyMCE
{
   /**
    * Constructor
    *
    * @return void
    */
    public function __construct()
    {
        // Actions
        add_action('admin_head', [$this, 'createButton']);
        add_action('wp_ajax_includes_request', [$this, 'includesAjaxRequest']);
        add_action('wp_ajax_nopriv_includes_request', [$this, 'includesAjaxRequest']);

        // Shortcode
        add_shortcode('pods_include', [$this, 'shortcodeInclude']);
    }

    /**
     * Create TinyMCE button
     *
     * @return void
     */
    public function createButton()
    {
        global $typenow;
        if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
            return;
        }

        add_filter("mce_external_plugins", [$this, "addScript"]);
        add_filter('mce_buttons', [$this, 'registerButton']);
    }


    /**
     * Add script to wp-admin
     *
     * @param array $plugin_array
     * @return array
     */
    public function addScript($plugin_array)
    {
        $plugin_array['add_script'] = plugin_dir_url(__FILE__) .'assets/scripts/main.js';
        return $plugin_array;
    }

    /**
     * Registers TinyMCE button
     *
     * @param  array $buttons
     * @return array
     */
    public function registerButton($buttons)
    {
        array_push($buttons, 'includes_button');
        return $buttons;
    }

    /**
     * WP Ajax request handler
     *
     * @return void sends json data to wp admin
     */
    public function includesAjaxRequest()
    {
        $dir = get_stylesheet_directory() .'/templates/**/*.php';
        $files = glob($dir);
        $list = [];

        foreach ($files as $file) {
            $template = $this->getFileDocBlock($file);
            $filepath = substr($file, strpos($file, get_stylesheet_directory()) + strlen(get_stylesheet_directory()));
            if ($template) {
                $list[] = [
                    'text' =>   $template[1],
                    'value' =>  $filepath
                ];
            }
        }

        wp_send_json($list);
    }

    /**
     * Get template name based on comment Doc Block in parenthesis
     * eg. (Template: Card)
     *
     * @param  string $file path to file
     * @return string       returns template name
     */
    private function getFileDocBlock($file)
    {
        $docComments = array_filter(
            token_get_all(file_get_contents($file)), function($entry) {
                return $entry[0] == T_DOC_COMMENT;
            }
        );
        $fileDocComment = array_shift($docComments);

        // Match pattern in parenthesis
        preg_match('#\((.*?)\)#', $fileDocComment[1], $templateName);
        return $templateName;
    }

    /**
     * PHP include file
     *
     * @param  array  $atts
     * @return void
     */
    public function shortcodeInclude($atts = [])
    {
        extract(
            shortcode_atts(['file' => 'NULL'], $atts)
        );

        // check for query string of variables after file path
        if (strpos($file, "?")) {
            global $query_string;
            $qs_position = strpos($file, "?");
            $qs_values = str_replace("amp;", "", substr($file, $qs_position + 1));

            parse_str($qs_values, $query_string);

            // Remove query string from file
            $file = substr($file, 0, $qs_position);
        }

        $filepath = get_stylesheet_directory() . $file;

        // check if the file was specified and if the file exists
        if ($file != 'NULL' && file_exists($filepath)) {
            // turn on output buffering to capture script output
            ob_start();

            // include the specified file
            include($filepath);

            // assign the file output to $content variable and clean buffer
            $content = ob_get_clean();

            return $content;
        }
    }
}

new TinyMCE;
